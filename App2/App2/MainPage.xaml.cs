﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App2
{
    public partial class MainPage : ContentPage
    {
      //touch recognition for when a gesture is recognised
        public MainPage()
        {
            InitializeComponent();
            
            var tgr = new TapGestureRecognizer();
            
            tgr.Tapped += async (sender, e) =>
            {
                Image image = sender as Image;
                image.Opacity = .5;
                await Task.Delay(200);
                image.Opacity = 1;
                await Navigation.PushAsync(new DetailPage1());
            };

            img11.GestureRecognizers.Add(tgr);
            img21.GestureRecognizers.Add(tgr);
            img31.GestureRecognizers.Add(tgr);
            img41.GestureRecognizers.Add(tgr);
            img51.GestureRecognizers.Add(tgr);

            img12.GestureRecognizers.Add(tgr);
            img22.GestureRecognizers.Add(tgr);
            img32.GestureRecognizers.Add(tgr);
            img42.GestureRecognizers.Add(tgr);
            img52.GestureRecognizers.Add(tgr);

            img13.GestureRecognizers.Add(tgr);
            img23.GestureRecognizers.Add(tgr);
            img33.GestureRecognizers.Add(tgr);
            img43.GestureRecognizers.Add(tgr);
            img53.GestureRecognizers.Add(tgr);

            img14.GestureRecognizers.Add(tgr);
            img24.GestureRecognizers.Add(tgr);
            img34.GestureRecognizers.Add(tgr);
            img44.GestureRecognizers.Add(tgr);
            img54.GestureRecognizers.Add(tgr);
        }
    }
}
