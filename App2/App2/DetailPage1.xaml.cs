﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace App2
{
    public partial class DetailPage1 : ContentPage
    {
        // Tap function allows for the image to redirect to the Detail page.
        public DetailPage1()
        {
            InitializeComponent();
        }

        async void OnBackClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}
